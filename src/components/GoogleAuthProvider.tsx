import jwtDecode from "jwt-decode";
import {
  createContext,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";

export type User = {
  jwt: string;
  email: string;
  name: string;
  picture: string;
  sub: string;
};

export const USER_CONTEXT = createContext<User | null>(null);

export const GOOGLE_CONTEXT = createContext(null);

export function useUser(): User | null {
  const user = useContext(USER_CONTEXT);
  return user;
}

export function useGoogle(): any {
  const google = useContext(GOOGLE_CONTEXT);
  return google;
}

function makeUser(jwt: any): User {
  let decoded = jwtDecode(jwt.credential) as any;
  console.log("RESPONSE!", decoded);
  return {
    jwt: jwt,
    email: decoded.email,
    name: decoded.name,
    picture: decoded.picture,
    sub: decoded.sub,
  };
}

type Props = {
  children: ReactNode;
};
export function GoogleScriptProvider(p: Props) {
  const [myGoogle, setGoogle] = useState<any>(null);
  const [jwt, setJwt] = useState<any>(null);

  const initialize = useCallback(
    (google: any) => {
      console.log(
        "Google script loaded",
        google,
        process.env.REACT_APP_CLIENT_ID
      );
      google.accounts.id.initialize({
        client_id: process.env.REACT_APP_CLIENT_ID,
        callback: (jwt: any) => {
          setJwt(makeUser(jwt));
        },
        auto_select: true,
        context: "signin",
        state_cookie_domain: "quotesc.vercel.app",
        allowed_parent_origin: [
          "http://localhost",
          "http://localhost:3000",
          "https://djehuti.home.obyoxion.at:3000/",
          "https://quotesc.vercel.app/",
        ],
        ux_mode: "popup",
      });
      setGoogle(google);
    },
    [setGoogle, setJwt]
  );

  useEffect(() => {
    let current = true;
    const head = document.getElementsByTagName("head")[0];
    const existing = document.getElementById("google-script");

    if (existing) {
      if (typeof google !== "undefined") {
        initialize(google);
      } else {
        console.log("React component changed, updating onload handler");
        existing.onload = () => {
          if (current) {
            initialize(google);
          } else {
            console.error(
              "Google script loaded, but onload handler belongs to a detached component. Provider cannot be set"
            );
          }
        };
      }
    } else {
      const script = document.createElement("script");
      console.log("Mounting Google script");

      script.src = "https://accounts.google.com/gsi/client";
      script.async = true;
      script.id = "google-script";
      script.referrerPolicy = "strict-origin-when-cross-origin";
      script.onload = () => {
        if (current) {
          initialize(google);
        } else {
          console.error(
            "Google script loaded, but onload handler belongs to a detached component. Provider cannot be set"
          );
        }
      };

      head.appendChild(script);

      return () => {
        current = false;
      };
    }
  }, [initialize]);

  return (
    <GOOGLE_CONTEXT.Provider value={myGoogle}>
      <USER_CONTEXT.Provider value={jwt}>{p.children}</USER_CONTEXT.Provider>
    </GOOGLE_CONTEXT.Provider>
  );
}

import { Button } from "@mui/material";
import {
  FunctionComponent,
  memo,
  ReactElement,
  ReactNode,
  useEffect,
  useRef,
} from "react";
import { useGoogle, useUser } from "./GoogleAuthProvider";
import UserMenu from "./UserMenu";

const LoginButton: FunctionComponent = () => {
  const ref = useRef<HTMLDivElement | null>(null);
  const google = useGoogle();
  const user = useUser();

  let link = "";

  console.log("R: ", document.referrer);

  useEffect(() => {
    if (user) {
    } else {
      if (google) {
        google.accounts.id.renderButton(ref.current, {
          theme: "outline",
          size: "large",
        });
        google.accounts.id.prompt(); // also display the One Tap dialog
      }
    }
  }, [google, user]);

  if (user) {
    return <UserMenu user={user}></UserMenu>;
  } else {
    return (
      <>
        <div ref={ref}></div>
      </>
    );
  }
};

export default memo(LoginButton);

import React from "react";
import logo from "./logo.svg";
import "./App.css";
import {
  AppBar,
  Box,
  Button,
  Container,
  IconButton,
  Paper,
  Stack,
  TextField,
  ThemeProvider,
  Toolbar,
  Typography,
} from "@mui/material";
import { theme } from "./theme";
import LoginButton from "./components/Login";
import { GoogleScriptProvider } from "./components/GoogleAuthProvider";

function SchwalbenButton(p: any) {
  return <button>Schwalben {p.text}</button>;
}
function ButtonAppBar() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Typography
            variant="h6"
            component="div"
            sx={{ fontWeight: "bold", mr: 2 }}
          >
            Quotesc
          </Typography>
          <Box sx={{ flexGrow: 1 }}>
            <Button color="inherit">List</Button>
          </Box>
          <LoginButton></LoginButton>
        </Toolbar>
      </AppBar>
    </Box>
  );
}

function App() {
  return (
    <>
      <GoogleScriptProvider>
        <ThemeProvider theme={theme}>
          <Box
            sx={{ backgroundColor: "background.default", minHeight: "100vh" }}
          >
            <ButtonAppBar></ButtonAppBar>
            <Container maxWidth="sm">
              <Paper sx={{ m: 2, p: 2 }}>
                <Container>
                  <Stack spacing={7}>
                    <Button variant="contained">Specht</Button>
                    <TextField label="Fill me" variant="filled" />
                  </Stack>
                </Container>
              </Paper>
            </Container>
          </Box>
        </ThemeProvider>
      </GoogleScriptProvider>
    </>
  );
}

export default App;

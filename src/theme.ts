import { createTheme, ThemeOptions } from "@mui/material";

const themeOptions: ThemeOptions = {
  palette: {
    primary: {
      main: "#3f774d",
    },
    secondary: {
      main: "#675645",
    },
    success: {
      main: "#7dca2b",
    },
    background: {
      default: "#a6d494",
    },
    error: {
      main: "#9a0b00",
    },
    divider: "#011001",
  },
  shape: {
    borderRadius: 2,
  },
};

export const theme = createTheme(themeOptions);

CREATE MIGRATION m1apclgz3655pxfa4ff3rowf6qy3534w22x6u4orkajttyu3bioqxa
    ONTO initial
{
  CREATE TYPE default::Quote {
      CREATE REQUIRED PROPERTY text -> std::str;
  };
};

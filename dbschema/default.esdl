using extension graphql;

module default {
    type Quote {
        required property text -> str;
    }
}
